<?php
  function door2door_styles(){
      wp_enqueue_style('style', get_stylesheet_uri());
  }

  function door2door_scripts() {
      wp_enqueue_script( 'navigator.js', get_template_directory_uri() . '/js/navigator.js', array( 'jquery' ));
  }

  add_action( 'wp_enqueue_scripts', 'door2door_scripts' );
  add_action('wp_enqueue_scripts', 'door2door_styles');
?>
