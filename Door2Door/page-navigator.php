<?php get_header() ?>

<h1>CONTACT NAVIGATOR</h1>
<div class="donor_list">
<?php
/* grab the posts from the contact list */
  $args = array(
    'post_type' => 'contact',
    'posts_per_page' => 20
  );
  /* begin looping through contacts */
  $donors = get_posts($args);
  if ($donors) {
    foreach ($donors as $donor) {
      /* set up convenience variables for displaying contact info */
      $donor_data = get_post_meta( $donor->ID );
      $donor_name = get_the_title($donor);
      $donor_id = $donor_data['donor_id'][0];
      $donor_phone = $donor_data['donor_phone'][0];
      $donor_address = $donor_data['donor_address'][0];
      $donor_last_gift = $donor_data['donor_last_gift'][0];
      $donor_gift_type = $donor_data['donor_last_gift_type'][0];

      ?>
      <!-- CONTACT HTML COMPONENT -->
      <section class="donor_wrapper" contact_id='<?php echo $donor_id?>'>
        <!-- Display Info -->
        <div class="info_wrapper">
          <div class="donor_name"><p><?php echo $donor_name?></p></div>
          <div class="donor_address"><p><?php echo $donor_address?></p></div>
          <div class="donor_phone"><p><?php echo $donor_phone?></p></div>
          <div class="donor_last_gift"><p><?php echo $donor_last_gift?></p></div>
          <div class="donor_gift_type"><p><?php echo $donor_gift_type?></p></div>
          <div class="knock">Knock</div>
        </div>
        <!-- Hidden Response Panel -->
        <div class="response_wrapper">
          <div class="contact response_button">Contact</div>
          <div class="not_home response_button">Not Home</div>
          <div class="try_later response_button">Try Later</div>
          <div class="bad_address response_button">Bad address</div>
          <div class="listed response_button">Listed</div>
          <div class="not_listed response_button">Not Listed</div>
          <div class="giving response_button">Giving</div>
          <div class="not_giving response_button">Not Giving</div>
          <div class="money response_button">Money</div>
          <div class="spouse response_button">Spouse</div>
          <div class="research response_button">Research</div>
        </div>
      </section>
      <!---->
      <?php
    }
  }
?>
</div>

<?php get_footer(); ?>
